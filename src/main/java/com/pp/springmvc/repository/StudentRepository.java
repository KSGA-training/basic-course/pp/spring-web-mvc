package com.pp.springmvc.repository;

import com.pp.springmvc.model.Student;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepository {
  public List<Student> studentList = new ArrayList<>();
  {
    studentList.add(new Student(1,"Chaihuo","Male"));
    studentList.add(new Student(2,"Santarak","Male"));
    studentList.add(new Student(3,"Leangkhim","Female"));
    studentList.add(new Student(4,"Chanry","Female"));

  }

  public List<Student> findAll(){
    return studentList;
  }

  public Student findOne(int id){
    return studentList.get(id-1);
  }

  public void addNewStudent(Student student){
    studentList.add(student);
  }



}
