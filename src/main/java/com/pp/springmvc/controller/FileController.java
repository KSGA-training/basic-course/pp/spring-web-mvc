package com.pp.springmvc.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileController {

  @Value("${file.server.path}")
  String serverPath;

  @Value("${url.image}")
  String imageUrl;
  @GetMapping("file/upload")
  public String showFormUplaod(){
    return "form-upload";
  }

  @PostMapping("/file/upload")

  public String handleUpload(@RequestPart("file") MultipartFile file){
    String originalfilename = file.getOriginalFilename();
    String afterSavedFilename = UUID.randomUUID().toString()+"."+originalfilename.substring(originalfilename.lastIndexOf(".")+1);
    if(!file.isEmpty()){

      try {
        Files.copy(file.getInputStream(), Paths.get(serverPath, afterSavedFilename));
      } catch (IOException e) {
        e.printStackTrace();
      }

    }
    System.out.println(imageUrl+afterSavedFilename);
    return "form-upload";
  }

}
