package com.pp.springmvc.controller.restcontroller;

import com.pp.springmvc.model.Student;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/students")
public class StudentRestController {
  public List<Student> studentList = new ArrayList<>();

  //TODO: Initialize data
  {
    studentList.add(new Student(1,"Chaihuo","Male"));
    studentList.add(new Student(2,"Santarak","Male"));
    studentList.add(new Student(3,"Leangkhim","Female"));
    studentList.add(new Student(4,"Chanry","Female"));

  }

  @GetMapping
  public List<Student> fetchAllStudent(){
    return studentList;
  }



  @GetMapping("/{studentId}")
  public Student findById(@PathVariable(name = "studentId") int id){
    Student student = studentList.get(id-1);
    return student;
  }
  @GetMapping("/search")
  public  List<Student> findByGender(@RequestParam String gender){

   List<Student> list = studentList.stream().filter(student -> student.getGender().equalsIgnoreCase(gender)).collect(
       Collectors.toList());


    return list;
  }
  @GetMapping("/show-index")
  public String showIndex(){
    return "index";
  }
}
