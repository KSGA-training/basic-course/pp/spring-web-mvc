package com.pp.springmvc.controller;

import com.pp.springmvc.model.Student;
import com.pp.springmvc.service.StudentService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

  private final StudentService studentService;
  @Autowired
  public HomeController(StudentService studentService) {
    this.studentService = studentService;
  }

  @GetMapping("/students")
  public String getIndexPage(ModelMap modelMap){
    String title = "KSGA- Student Management";
    modelMap.addAttribute("title",title);
    modelMap.addAttribute("studentList",studentService.findAll());
    return "index";
  }

  @GetMapping("/students/register")
  public String showFormAdd(){
    return "form-add";
  }

  @PostMapping("/students/add")
  public String addStudent(@ModelAttribute @Valid Student student, BindingResult result){
    if(result.hasErrors()){
      System.out.println(result.getFieldErrors().toString());
      return "redirect:/students/register";
    }
    student.setId(studentService.findAll().size()+1);
    studentService.addNewStudent(student);
    return "redirect:/students";
  }



}
