package com.pp.springmvc.configuration;

import java.util.Locale;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
public class MultipleLanguageConfiguration implements WebMvcConfigurer {

  @Bean
  public LocaleResolver localeResolver(){
    SessionLocaleResolver resolver= new SessionLocaleResolver();
    resolver.setDefaultLocale(Locale.US);
    return resolver;
  }
  @Bean
  public LocaleChangeInterceptor localeChangeInterceptor(){
    LocaleChangeInterceptor local = new LocaleChangeInterceptor();
    local.setParamName("lang");
    return local;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(localeChangeInterceptor());
  }
}
