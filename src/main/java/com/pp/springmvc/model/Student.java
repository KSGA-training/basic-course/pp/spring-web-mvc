package com.pp.springmvc.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {


  private int id;

  @NotNull
  @NotEmpty(message = "Nmae can't be empty")
  private String name;
  @NotEmpty
  @NotNull(message = "Gender can't be null")
  private String gender;


}
