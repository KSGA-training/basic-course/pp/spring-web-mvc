package com.pp.springmvc.service;

import com.pp.springmvc.model.Student;
import java.util.List;

public interface StudentService {

  public List<Student> findAll();
  public Student findOne(int id);
  public void addNewStudent(Student student);

}
