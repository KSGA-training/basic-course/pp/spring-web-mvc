package com.pp.springmvc.service.imp;

import com.pp.springmvc.repository.StudentRepository;
import com.pp.springmvc.model.Student;
import com.pp.springmvc.service.StudentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImp implements StudentService {
private StudentRepository studentRepository;
@Autowired
  public StudentServiceImp(StudentRepository studentRepository) {
    this.studentRepository = studentRepository;
  }

  @Override
  public List<Student> findAll() {
    return studentRepository.findAll();
  }

  @Override
  public Student findOne(int id) {
    return studentRepository.findOne(id);
  }

  @Override
  public void addNewStudent(Student student) {
studentRepository.addNewStudent(student);
  }
}
